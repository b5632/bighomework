#include "link.h"
#include <stdlib.h>
#include <stdio.h>
int greater(Elem x,Elem y){
    return x.x>y.x;
}
int less(Elem x,Elem y){
    return x.x<y.x;
}
void printElem(Elem x){
    printf("%d",x.x);
}
queue* init(){
    queue *q=(queue*)malloc(sizeof(queue));
    if(q==NULL)return (queue*)NULL;
    q->head=(node*)malloc(sizeof(node));
    q->tail=q->head;
    q->cmp=NULL;
    if(q->head==NULL){
        free(q);
        return NULL;
    }
    q->size=0;
    q->head->next=NULL;
    return q;
}
queue* initSortedLink(int (*cmp)(Elem,Elem)){
    queue *q=init();
    if(q==NULL)return NULL;
    q->cmp=cmp;
    return q;
}
int frontInsert(queue *q,Elem e){
    node *nptr = (node*)malloc(sizeof(node));
    if(nptr==NULL){
        return NO_MEM_ALLOC;
    }
    node *tmp=q->head->next;
    q->head->next=nptr;
    nptr->e=e;
    nptr->next=tmp;
    if(tmp==NULL){
        q->tail=nptr;
    }
    q->size++;
    return SUCCESS;
}
int backInsert(queue *q,Elem e){
    q->tail->next=(node*)malloc(sizeof(node));
    if(q->tail->next==NULL)return NO_MEM_ALLOC;
    q->tail=q->tail->next;
    q->tail->e=e;
    q->tail->next=NULL;
    q->size++;
    return SUCCESS;
}
int Insert(queue* q,int x,Elem e){
    if(x>q->size||x<=0){
        return FAILURE;
    }
    if(x==q->size){
        return backInsert(q,e);
    }
    node *nPtr = (node*)malloc(sizeof(node));
    if(nPtr==NULL)return NO_MEM_ALLOC;
    node *tmp=q->head;
    for(int i=1;i<x;i++){
        tmp=tmp->next;
    }
    nPtr->e=e;
    nPtr->next=tmp->next;
    q->size++;
    tmp->next=nPtr;
    return SUCCESS;
}
int sortedInsert(queue *q,Elem e){
    if(q->cmp==NULL)return NOT_SORTED_LINK;
    node *nPtr=(node*)malloc(sizeof(node));
    nPtr->e=e;
    if(nPtr==NULL)return NO_MEM_ALLOC;
    node *tmp=q->head;
    while(tmp->next!=NULL){
        if(!q->cmp(e,tmp->next->e))break;
        tmp=tmp->next;
    }
    nPtr->next=tmp->next;
    tmp->next=nPtr;
    q->size++;
    if(tmp==q->tail){
        q->tail=nPtr;
    }
    return SUCCESS;
}
Elem remove(queue *q,int x){
    if(x<=0||x>q->size)return (Elem){0};
    node *iter=q->head;
    for(int i=1;i<x&&iter->next!=q->tail;i++){
        iter=iter->next;
    }
    node *tmp=iter->next;
    if(tmp->next==q->tail){
        q->tail=iter;
    }
    iter->next=iter->next->next;
    Elem ret=tmp->e;
    free(tmp);
    q->size--;
    return ret;
}
void printQueue(queue *q){
    node *iter=q->head->next;
    while(iter!=NULL){
        printElem(iter->e);
        if(iter!=q->tail){
            printf("->");
        }else{
            printf("\n");
        }
        iter=iter->next;
    }
}
void freeQueue(queue *q){
    node *iter=q->head->next;
    while(iter!=NULL){
        node *tmp=iter;
        iter=iter->next;
        free(tmp);
    }
    q->tail=q->head;
    q->size=0;
}