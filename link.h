#define NO_MEM_ALLOC -1
#define FAILURE 1
#define SUCCESS 0
#define NOT_SORTED_LINK 2
typedef struct ElemType{
    int x;
}Elem;
int greater(Elem x,Elem y);//x>y返回True
int less(Elem x,Elem y);//x<Y返回True
void printElem(Elem x);//输出元素x
typedef struct Node{
    Elem e;
    struct Node *next;
}node;
typedef struct Link{
    node *head;
    node *tail;
    int size;
    int (*cmp)(Elem,Elem);//查看是否已排序 0为无序 1为升序 2为降序
}queue;
queue* init();//初始化链表,返回一个空队列,该队列无序
queue* initSortedLink(int (*cmp)(Elem,Elem));//初始化一个有序队列,传入greater为降序，传入less为升序
//以下为无序队列的操作，若对有序队列执行以下操作，则队列变为无序
int frontInsert(queue* q,Elem e);//头插
int backInsert(queue* q,Elem e);//尾插
int Insert(queue* q,int x,Elem e);//指定插
queue* insertSort(queue *q,int (*cmp)(Elem,Elem));//插入排序
//以下为有序队列的操作，无序队列若执行此操作会报错
int sortedInsert(queue *q,Elem e);//有序插
//以下为无序和有序队列通用的操作
Elem remove(queue *q,int x);//删除第x个元素
int findPos(queue *q,Elem x);//寻找链表中是否存在x，若存在返回位置，否则返回-1（优先返回靠近队首的）
Elem selectPrior(queue *q);//寻找在cmp判断条件下优先级最高的元素(优先返回靠近队首的)
void printQueue(queue *q);//输出队列
void freeQueue(queue *q);//将队列设为空